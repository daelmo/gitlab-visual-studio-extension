﻿using Autofac;
using NUnit.Framework;
using System;
using GitLab.Extension.SettingsUtil;
using Microsoft.Win32;
using Serilog.Events;
using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    public class SettingsTests : TestBase
    {
        private bool _onSettingsChangedEventCalled = false;
        private Settings.SettingsEventArgs _onSettingsChangedEventArgs;
        private ISettingsProtect _settingsProtect;
        private ISettings _settings;
        private IObserver<TokenValidationNotification> _tokenValidationNotificationObserver;

        [SetUp]
        public void Setup()
        {
            _onSettingsChangedEventCalled = false;
            _onSettingsChangedEventArgs = null;

            CreateBuilder()
                .RegisterLogging()
                .RegisterSettings()
                .RegisterObservables()
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
            _settingsProtect = _scope.Resolve<ISettingsProtect>();
            _tokenValidationNotificationObserver = _scope.Resolve<IObserver<TokenValidationNotification>>();
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings(_settings);
        }

        #region Helper methods

        public class TestNullProtect : ISettingsProtect
        {
            public string Protect(string data)
            {
                return data;
            }

            public string Unprotect(string protectedData)
            {
                return protectedData;
            }
        }

        private void OnSettingsChangedEvent(object sender, EventArgs e)
        {
            _onSettingsChangedEventArgs = e as Settings.SettingsEventArgs;
            _onSettingsChangedEventCalled = true;
        }

        private static bool DoesRegistryKeyExist(string registryKey)
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{registryKey}");
            return key != null;
        }

        private string Protect(string data)
        {
            return _settingsProtect.Protect(data);
        }
        
        #endregion


        [Test]
        public void ConfiguredTest()
        {
            _settings.GitLabAccessToken = string.Empty;
            Assert.IsFalse(_settings.Configured, "Expected Configured to return false when access token isn't set");
            _settings.GitLabAccessToken = "glpat-abcdef";
            Assert.IsTrue(_settings.Configured, "Expected Configured to return true when access token is set");
        }

        [Test]
        public void SettingsChangedEventTest()
        {
            _settings.SettingsChangedEvent += OnSettingsChangedEvent;
            try
            {
                _settings.GitLabAccessToken = "glpat-abcdef";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabAccessTokenKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabAccessTokenKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                _settings.IsCodeSuggestionsEnabled = false;
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.IsCodeSuggestionsEnabledKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.IsCodeSuggestionsEnabledKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                _settings.IsTelemetryEnabled = false;
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.IsTelemetryEnabledKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.IsTelemetryEnabledKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                _settings.GitLabUrl = "https://abc.com";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabUrlKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabUrlKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");
            }
            finally
            {
                _settings.SettingsChangedEvent -= OnSettingsChangedEvent;
            }
        }

        [TestCase(Settings.GitLabAccessTokenKey, "12345", "Expect RegistryStorage to store correct access token")]
        [TestCase(Settings.IsTelemetryEnabledKey, false, "Expect RegistryStorage to store correct telemetry setting")]
        [TestCase(Settings.IsCodeSuggestionsEnabledKey, false, "Expect RegistryStorage to store correct code suggestion setting")]
        [TestCase(Settings.GitLabUrlKey, "https://test.gitlab.com", "Expect RegistryStorage to store correct GitLab URL")]
        [TestCase(Settings.LogLevelKey, LogEventLevel.Error, "Expect RegistryStorage to store correct log level")]
        public void RegistryStorage_Save_ShouldSaveToRegistry(
            string keyName,
            object expectedValue,
            string message)
        {
            var settings = _settings as Settings;

            switch (keyName)
            {
                case Settings.GitLabAccessTokenKey:
                    settings.GitLabAccessToken = (string)expectedValue;
                    break;
                case Settings.IsTelemetryEnabledKey:
                    settings.IsTelemetryEnabled = (bool)expectedValue;
                    break;
                case Settings.IsCodeSuggestionsEnabledKey:
                    settings.IsCodeSuggestionsEnabled = (bool)expectedValue;
                    break;
                case Settings.GitLabUrlKey:
                    settings.GitLabUrlValue = (string)expectedValue;
                    break;
                case Settings.LogLevelKey:
                    settings.LogLevel = (LogEventLevel)expectedValue;
                    break;
                default:
                    throw new ArgumentException($"Unsupported keyName: {keyName}");
            }

            if (DoesRegistryKeyExist(Settings.ApplicationName))
                Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationName}");

            settings.Storage.Save(settings);

            var actualValue = GetActualValue();
            Assert.AreEqual(expectedValue, actualValue, message);

            object GetActualValue()
            {
                using (var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}"))
                {
                    var registryValue = key.GetValue(keyName);


                    switch (keyName)
                    {
                        case Settings.GitLabAccessTokenKey:
                            return _settingsProtect.Unprotect((string) registryValue);
                        case Settings.LogLevelKey:
                            Enum.TryParse<LogEventLevel>((string) registryValue, out var parsedLogLevel);
                            return parsedLogLevel;
                        case Settings.IsTelemetryEnabledKey:
                            return (int)registryValue == 1;
                        case Settings.IsCodeSuggestionsEnabledKey:
                            return (int)registryValue == 1;
                        default:
                            return registryValue;
                    }
                }
            }
        }

        [TestCase(Settings.GitLabAccessTokenKey, "12345", "Expect correct GitLab access token to be loaded")]
        [TestCase(Settings.GitLabUrlKey, "test.gitlabtest.com", "Expect correct GitLab URL to be loaded")]
        [TestCase(Settings.IsTelemetryEnabledKey, false, "Expect correct telemetry enabled setting to be loaded")]
        [TestCase(Settings.IsCodeSuggestionsEnabledKey, false, "Expect correct code suggestions setting to be loaded")]        
        [TestCase(Settings.LogLevelKey, LogEventLevel.Debug, "Expect correct log level to be loaded")]
        public void RegistryStorage_Load_ShouldLoadFromRegistry(
            string keyName,
            object expectedValue,
            string message)
        {
            var settings = _settings as Settings;
            var storage = settings.Storage;

            using (var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", true))
            {
                switch (keyName)
                {
                    case Settings.GitLabAccessTokenKey:
                        key.SetValue(keyName, _settingsProtect.Protect((string) expectedValue), RegistryValueKind.String);
                        break;
                    case Settings.IsTelemetryEnabledKey:
                        key.SetValue(keyName, (bool) expectedValue == true ? 1 : 0, RegistryValueKind.DWord);
                        break;
                    case Settings.IsCodeSuggestionsEnabledKey:
                        key.SetValue(keyName, (bool) expectedValue == true ? 1 : 0, RegistryValueKind.DWord);
                        break;
                    default:
                        key.SetValue(keyName, expectedValue, RegistryValueKind.String);
                        break;
                }
            }

            storage.Load(settings);

            var actualValue = GetActualValue();
            Assert.AreEqual(expectedValue, actualValue, message);

            object GetActualValue()
            {
                switch (keyName)
                {
                    case Settings.GitLabAccessTokenKey:
                        return settings.GitLabAccessToken;
                    case Settings.GitLabUrlKey:
                        return settings.GitLabUrl;
                    case Settings.IsTelemetryEnabledKey:
                        return settings.IsTelemetryEnabled;
                    case Settings.IsCodeSuggestionsEnabledKey:
                        return settings.IsCodeSuggestionsEnabled;
                    case Settings.LogLevelKey:
                        return settings.LogLevel;
                    default:
                        throw new InvalidOperationException($"Unsupported keyName: {keyName}");
                }
            }
        }

        [Test]
        public void RegistryStorage_Load_WithPoCRegistryKeySetup_ShouldMigrateData()
        {
            if(DoesRegistryKeyExist(Settings.ApplicationName))
                Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationName}");

            // setup test values to put into poc registry key
            var tokenValue = Protect(TestData.CodeSuggestionsToken);
            var isCodeSuggestionsEnabledValue = 0;

            using (var pocKey = Registry.CurrentUser.CreateSubKey($"Software\\{Settings.ApplicationNamePoC}", true))
            {
                pocKey.SetValue(
                    Settings.GitLabAccessTokenKey,
                    tokenValue,
                    RegistryValueKind.String);

                pocKey.SetValue(
                    Settings.IsCodeSuggestionsEnabledKey,
                    isCodeSuggestionsEnabledValue,
                    RegistryValueKind.DWord);
            }

            Assert.IsTrue(
                DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expect PoC registry key to exist");

            Assert.IsFalse(
                DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to not exist");

            var settings = _settings as Settings;
            settings.Storage.Load(settings);

            Assert.IsFalse(
                DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expect PoC registry key to have been deleted");

            Assert.IsTrue(
                DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to exist");

            using (var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", false))
            {
                var newTokenValue = (string) key.GetValue(Settings.GitLabAccessTokenKey);
                Assert.AreEqual(
                    _settingsProtect.Unprotect(tokenValue),
                    _settingsProtect.Unprotect(newTokenValue), 
                    "Expect stored token values to be the same before and after migration");

                var newIsCodeSuggestionsEnabledValue = (int) key.GetValue(Settings.IsCodeSuggestionsEnabledKey);
                Assert.AreEqual(
                    isCodeSuggestionsEnabledValue, 
                    newIsCodeSuggestionsEnabledValue, 
                    "Expect stored telemetry enabled value to be the same before and after migration");
            }
        }

        [Test]
        public void Configured_IsFalse_WhenTokenNotificationReceived()
        {
            _tokenValidationNotificationObserver.OnNext(new TokenValidationNotification());
            Assert.IsFalse(_settings.Configured);
        }

        [Test]
        public void Configured_IsTrue_WhenSettingsAreChanged()
        {
            // simulate settings changed without actually changing settings
            _settings.StartBatchSettingsUpdate();
            _settings.StopBatchSettingsUpdate();

            Assert.IsTrue(_settings.Configured);
        }
    }
}
