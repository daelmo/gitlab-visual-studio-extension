﻿using Microsoft.VisualStudio.Shell;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests
{
    public class ExecuteOnMainThreadAttribute : Attribute, IWrapTestMethod
    {
        public TestCommand Wrap(TestCommand command)
        {
            return new ExecuteOnMainThreadCommand(command);
        }

        private class ExecuteOnMainThreadCommand : DelegatingTestCommand
        {
            public ExecuteOnMainThreadCommand(
                TestCommand command
            ) : base(command)
            { }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "VSTHRD002:Avoid problematic synchronous waits", Justification = "Setting Up Thread Environment")]
            public override TestResult Execute(
                TestExecutionContext context)
            {
                var task = RunOnMainThreadAsync(context);
                task.Wait();

                if (task.Exception != null)
                {
                    throw task.Exception.InnerException;
                }

                var result = task.Result;

                if (!result)
                {
                    context.CurrentResult.SetResult(
                        ResultState.Failure,
                        "Test did not pass when run on the main thread.");
                }

                return context.CurrentResult;
            }

            private async Task<bool> RunOnMainThreadAsync(
                TestExecutionContext context)
            {
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

                return innerCommand.Execute(context).ResultState.Status == TestStatus.Passed;
            }
        }
    }
}
