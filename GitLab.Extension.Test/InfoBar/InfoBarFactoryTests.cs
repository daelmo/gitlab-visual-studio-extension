﻿using GitLab.Extension.InfoBar;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Moq;
using NUnit.Framework;
using Serilog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.InfoBar
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods", Justification = "Ending in async confuses naming")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "VSTHRD010:Invoke single-threaded types on Main thread", Justification = "We are mocking IVsInfoBarHost")]
    public class InfoBarFactoryTests
    {
        private Mock<IVsInfoBarHost> _infoBarHost;
        private Mock<ILogger> _logger;

        private Mock<SVsInfoBarUIFactory> _vsInfoBarUIFactoryMock;
        private Mock<IVsInfoBarUIElement> _vsInfoBarUIElementMock;

        private InfoBarFactory _infoBarFactory;

        [SetUp]

        public void SetUp()
        {
            _infoBarHost = new Mock<IVsInfoBarHost>();
            _logger = new Mock<ILogger>();

            _vsInfoBarUIElementMock = new Mock<IVsInfoBarUIElement>();

            _vsInfoBarUIFactoryMock = new Mock<SVsInfoBarUIFactory>();
            _vsInfoBarUIFactoryMock
                .As<IVsInfoBarUIFactory>()
                .Setup(x => x.CreateInfoBar(It.IsAny<InfoBarModel>())).Returns(_vsInfoBarUIElementMock.Object);

            AssemblySetup.MockServiceProvider.Reset();
            AssemblySetup.MockServiceProvider.AddService(typeof(SVsInfoBarUIFactory), _vsInfoBarUIFactoryMock.Object);

            _infoBarFactory =
                new InfoBarFactory(
                    _logger.Object);
        }

        [Test]
        [ExecuteOnMainThread]
        
        public void AttachInfoBar_CallsAddInfoBarOnHost()
        {
            // Arrange
            var message = "Test message";
            var actions = new List<InfoBarAction>();

            // Act
            _infoBarFactory.AttachInfoBar(_infoBarHost.Object, message, actions);

            // Assert
            _infoBarHost.Verify(host  => host.AddInfoBar(It.IsAny<IVsInfoBarUIElement>()), Times.Once());
        }

        [Test, NonParallelizable]
        [ExecuteOnMainThread]
        public void AttachInfoBar_DoesNotCallAddInfoBar_WhenCreateInfoBarReturnsNull()
        {
            // Arrange
            AssemblySetup.MockServiceProvider.Reset();

            var uiFactoryMock = new Mock<SVsInfoBarUIFactory>();
            uiFactoryMock.As<IVsInfoBarUIFactory>().Setup(x => x.CreateInfoBar(It.IsAny<InfoBarModel>())).Returns(() => null);

            AssemblySetup.MockServiceProvider.AddService(typeof(SVsInfoBarUIFactory), uiFactoryMock.Object);

            // Act
            _infoBarFactory.AttachInfoBar(_infoBarHost.Object, "Test message", new List<InfoBarAction>());

            // Assert
            _infoBarHost.Verify(host => host.AddInfoBar(It.IsAny<IVsInfoBarUIElement>()), Times.Never());
        }
    }
}
