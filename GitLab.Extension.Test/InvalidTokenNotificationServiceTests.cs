﻿using EnvDTE;
using GitLab.Extension.InfoBar;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    public class InvalidTokenNotificationServiceTests
    {
        private Mock<ISettings> _settingsMock;
        private Mock<IInfoBarFactory> _infoBarFactoryMock;
        private Mock<IInfoBar> _infoBarMock;
        private Subject<TokenValidationNotification> _tokenValidationNotifications;

        private InvalidTokenNotificationService _service;

        [SetUp]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "VSTHRD010:Invoke single-threaded types on Main thread", Justification = "<Pending>")]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
            _infoBarFactoryMock = new Mock<IInfoBarFactory>();
            _infoBarMock = new Mock<IInfoBar>();

            _infoBarFactoryMock
                .Setup(factory => factory
                    .AttachInfoBar(
                        It.IsAny<IVsInfoBarHost>(), 
                        It.IsAny<string>(), 
                        It.IsAny<IEnumerable<InfoBarAction>>(), 
                        It.IsAny<ImageMoniker>()))
                .Returns(_infoBarMock.Object);

            _tokenValidationNotifications = new Subject<TokenValidationNotification>();

            var mockHost = new Mock<IVsInfoBarHost>();
            var mockShell = new Mock<IVsShell>();
            object _host = mockHost.Object;
            mockShell.Setup(shell => shell.GetProperty((int)__VSSPROPID7.VSSPROPID_MainWindowInfoBarHost, out _host))
                .Returns(VSConstants.S_OK);

            AssemblySetup.MockServiceProvider.Reset();
            AssemblySetup.MockServiceProvider.AddService(typeof(IVsShell), mockShell.Object);

            _service = new InvalidTokenNotificationService(
                _settingsMock.Object, 
                _infoBarFactoryMock.Object, 
                _tokenValidationNotifications);
        }

        [Test]
        [ExecuteOnMainThread]
        public void HandleTokenValidationNotification_AttachesInfoBar_WithCorrectMessage()
        {
            // Act
            _tokenValidationNotifications.OnNext(new TokenValidationNotification());

            // Assert
            _infoBarFactoryMock.Verify(factory => factory.AttachInfoBar(
                It.IsAny<IVsInfoBarHost>(),
                InvalidTokenNotificationService.INVALID_ACCESS_TOKEN_MESSAGE,
                It.IsAny<IEnumerable<InfoBarAction>>(),
                It.IsAny<ImageMoniker>()
            ), Times.Once());
        }

        [Test]
        [ExecuteOnMainThread]
        public void HandleTokenValidationNotification_DoesNotAttachNewInfoBar_IfOneExists()
        {
            // Arrange
            _tokenValidationNotifications.OnNext(new TokenValidationNotification()); // Create an InfoBar

            // Act
            _tokenValidationNotifications.OnNext(new TokenValidationNotification()); // Try to create another InfoBar

            // Assert
            _infoBarFactoryMock.Verify(factory => 
                factory.AttachInfoBar(
                    It.IsAny<IVsInfoBarHost>(),
                    InvalidTokenNotificationService.INVALID_ACCESS_TOKEN_MESSAGE, 
                    It.IsAny<IEnumerable<InfoBarAction>>(),
                    It.IsAny<ImageMoniker>()
                ), Times.Once());
        }

        [Test]
        public void HandleSettingsUpdated_ClosesInfoBar()
        {
            // Arrange
            _tokenValidationNotifications.OnNext(new TokenValidationNotification()); // Create an InfoBar

            // Act
            _settingsMock.Raise(s => s.SettingsChangedEvent += null, new EventArgs());

            // Assert
            _infoBarMock.Verify(infoBar => infoBar.Close(), Times.Once());
        }

        [Test, NonParallelizable]
        public void Dispose_RemovesSettingsChangedEventHandler()
        {
            // Act
            _service.Dispose();

            // Assert
            _settingsMock.Raise(s => s.SettingsChangedEvent += null, new EventArgs());
            _infoBarMock.Verify(infoBar => infoBar.Close(), Times.Never());
        }
    }
}
