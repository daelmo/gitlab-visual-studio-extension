﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GitLab.Extension.Tests
{
    /// <summary>
    /// A test http listener. response.close is called automatically.
    /// </summary>
    /// <remarks>
    /// To send a body of data:
    /// 
    /// byte[] data = Encoding.UTF8.GetBytes("hello world");
    /// resp.ContentType = "text/html";
    /// resp.ContentEncoding = Encoding.UTF8;
    /// resp.ContentLength64 = data.LongLength;
    /// 
    /// // Write out to the response stream (asynchronously)
    /// await resp.OutputStream.WriteAsync(data, 0, data.Length);
    /// </remarks>
    public class TestHttpListener : IDisposable
    {
        private bool disposedValue;
        private HttpListener _listener;
        private Action<HttpListenerRequest, HttpListenerResponse> _onRequestHandler;
        private CancellationTokenSource _cancellationTokenSource;
        private string _url;

        public TestHttpListener(Action<HttpListenerRequest, HttpListenerResponse> onRequestHandler)
        {
            var port = GetAvailablePort();
            _url = $"http://127.0.0.1:{port}/";
            _cancellationTokenSource = new CancellationTokenSource();
            _onRequestHandler = onRequestHandler;

            _listener = new HttpListener();
            _listener.Prefixes.Add(_url);
            _listener.Start();

            // Handle connections in the background
            _ = HandleIncomingConnectionsAsync(_cancellationTokenSource.Token);
        }

        public string Url { get { return _url; } }

        private async Task HandleIncomingConnectionsAsync(CancellationToken token)
        {
            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (!token.IsCancellationRequested)
            {
                // Will wait here until we hear from a connection
                HttpListenerContext ctx = await _listener.GetContextAsync();

                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;

                _onRequestHandler(req, resp);

                resp.Close();
            }
        }

        private static int GetAvailablePort()
        {
            var loopbackEndpoint = new IPEndPoint(IPAddress.Loopback, port: 0);
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(loopbackEndpoint);
                return ((IPEndPoint)socket.LocalEndPoint).Port;
            }
        }

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _cancellationTokenSource.Cancel();
                    _cancellationTokenSource = null;
                    _listener.Close();
                    _listener = null;
                    _onRequestHandler = null;
                }

                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~TestHttpListener()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
