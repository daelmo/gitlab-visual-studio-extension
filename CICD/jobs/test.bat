
::
:: Variables
set nunit=C:\ProgramData\chocolatey\lib\nunit-console-runner\tools\nunit3-console.exe
set altcover_version=8.6.68
set altcover=altcover.%altcover_version%\tools\net472\AltCover.exe

@echo off
if "%GITLAB_TOKEN%"=="" (
    @echo Required variable GITLAB_TOKEN is NOT defined
    exit /b 1
)

if "%TEST_FOLDER%"=="" (
    @echo Required variable TEST_FOLDER is NOT defined
    exit /b 1
)
@echo on

::
:: Install needed packages
choco install nunit-console-runner --force --passive -y
if %errorlevel% neq 0 exit /b %errorlevel%

choco install nuget.commandline --force --passive -y
if %errorlevel% neq 0 exit /b %errorlevel%

choco install sed --force --passive -y
if %errorlevel% neq 0 exit /b %errorlevel%

nuget install altcover -Version %altcover_version%
if %errorlevel% neq 0 exit /b %errorlevel%

::
:: Inject coverage into built assemblies
%altcover% -c=coverage_report.xml ^
  -i=%TEST_FOLDER% ^
  --sn=GitLab.Extension.Test\key.snk ^
  -s=Adapter -s=.Recorder -s=nunit ^
  -s=System. -s=Microsoft. ^
  -t=System. -t=Microsoft.
if %errorlevel% neq 0 exit /b %errorlevel%

::
:: Run NUnit tests w/coverage
%altcover% Runner ^
  -r=%TEST_FOLDER%\__Instrumented ^
  -c=coverage_report.xml ^
  -x=%nunit% ^
  -- ^
    %TEST_FOLDER%\__Instrumented\GitLab.Extension.Tests.dll ^
    --result=TestResult.xml ^
    --result=junit-results.xml;transform=CICD\nunit3-junit.xslt
if %errorlevel% neq 0 exit /b %errorlevel%

::
:: Remove source path prefix from coverage report
sed -i -f CICD/coverage_report.sed ^
    coverage_report.xml
if %errorlevel% neq 0 exit /b %errorlevel%

:: end
