
Developer documentation for the Visual Studio extension

- [Development Process](development-process.md)
  - Environment setup
- [Architecture](architecture.md)
  - Code organization
  - Discussion about various components
  - Architecture diagrams
- [How Proposals Work in Visual Studio](how-proposals-work.md)
  - Proposals is VS terminology for inline suggestions
  - Entrypoints into our extension
- [Release Process](release-process.md)
