# How Visual Studio Proposals Work

Documenting information about how proposals in Visual Studio work.
Proposals are Visual Studio's name for inline code suggestions.

## Assemblies

Assemblies involved in proposals.

### `Microsoft.VisualStudio.Language`

This assembly contains the definitions for Proposal related
classes in the `Microsoft.VisualStudio.Langauge.Proposals` namespace.

There is little to no actual code in this assembly.

### `Microsoft.VisualStudio.IntelliCode`

This assembly contains an implementation of `Microsoft.VisualStudio.Langauge.Proposals`.
IntelliCode provides the base implementation for AI code suggestions using local models.
It supports C# and C++.

## Diagrams

### Document is opened

Triggered by a user opening a file in Visual Studio 2022:

```plaintext
Text.Editor.Implementation.WpfTextView.BindContentSpecificAssets
->
  IntelliCode...SuggestionService.InlineCompletion.InlineCompletionInstance.Create
  -> Singleton instance per-IWpfTextView
  -> .ctor
  -> IntelliCode...ProposalBrokerProvider.GetProposalBroker
     -> Singleton instance per-IWpfTextView
     -> IntelliCode...ProposalBroker.ctor
        -> IntelliCode...ProposalBroker.AquireSources
           -> GitlabProposalManagerProvider.ctor
           -> GitlabProposalManagerProvider.GetProposalManagerAsync
              -> GitlabProposalManager.ctor
        -> GitLabProposalSourceProvider.ctor
        -> GitLabProposalSourceProvider.GetProposalSourceAsync
```

## Things to keep in mind

### Loading order of exported classes

Because IntelliCode has an implementation of proposals, some of our exported classes must
be configured to load before the IntelliCode ones.

For example, `GitlabProposalManagerProvider` is configured to load before its IntelliCode counterpart.
This is configured using the `Order` assembly assertion.

```csharp
[Export(typeof(GitlabProposalManagerProvider))]
[Export(typeof(ProposalManagerProviderBase))]
[Name("GitlabProposalManager")]
[ContentType("any")]
[Order(Before = "IntelliCodeCSharpProposalManager")]
[Order(Before = "Highest Priority")]
public class GitlabProposalManagerProvider : ProposalManagerProviderBase
```

### Leveraging IntelliCode's implementation

Often we can skip over implementing a proposal class until we find a limitation
with Visual Studio's implementation. For example, initially, we didn't implement a
`ProposalManager` class. Later we found it was blocking us from supporting a wider
number of languages that didn't have IntelliCode support.

When a limitation is hit, part of the investigation should be to determine if one
of the IntelliCode implemented classes we leverage is causing the limitation. We
can then implement another proposal class ourselves to get around the limitation.
