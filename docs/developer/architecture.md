# Extension architecture

This document gives you a high-level overview of the main components of the GitLab Extension for Visual Studio. It helps you place your contribution in the right place in the code base.

## Code Organization

- `GitLab.Extension/`
  - `AutofacSerilogIntegration/` -- 3rd party component for providing logging from dependency injection library
  - `CodeSuggestions/` -- Code to interface with Visual Studio and provide code suggestions
  - `Constants/` -- Globally accessible constants
  - `Command/` -- Code to declare and register command handlers with Visual Studio
  - `InfoBar/` -- InfoBar Integration
  - `LanguageServer/` -- Code to interface with Language Server
  - `Resources/` -- Binary resources (images, language server, etc.)
  - `Serilog.Enrichers.Sensitive/` -- 3rd party component to mask sensitive information like personal access tokens from logs
  - `Serilog.Sink.VsOutput/` -- Display logs to Visual Studio output window
  - `SettingsUtil/` -- Extension settings
  - `Status/` -- Status bar integration
  - `DependencyInjection.cs` -- Configure dependency injection
  - `InvalidTokenNotificationService.cs` -- Handles notifying users when we are informed that the configured PAT is invalid
  - `Logging.cs` -- Configure logging
  - `Metrics.cs` -- Collect metrics (telemetry) about extension usage
  - `VisualStudioPackage.cs` -- Handle VS events like `SolutionEvents` for initialization/cleanup
- `GitLab.Extension.Tests/` -- Automated tests

## Components

### Dependency Injection

This project uses the [autofac](https://autofac.org/) dependency injection library.
The last version to support .NET Framework is used. Make sure when reading documentation
it's the version in use, not the latest.

Many of our codes entrypoints are called from Visual Studio; to support dependency injection
a singleton instance of `GitLab.Extension.DependencyInjection` is created and accessible from
`GitLab.Extension.DependencyInjection.Instance`. This then exposes an autofac `ILifetimeScope`
that is used to resolve objects/interfaces.

To register a new interface/class, update the `DependencyInjection.RegisterComponents` method.

### Settings

Settings are stored in the Windows Registry and edited through the main menu in Visual Studio via **Tools > Options**. More instructions can be found in the [main README](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/README.md#setup).

Sensitive settings are encrypted using the Windows Data Protection API. See `SettingsUtil/ProtectImpl.cs`.

Settings components:

- `ISettings` -- Component providing access (get/set) to the settings themselves.
  - `Settings` -- Implementation.
- `ISettingsStorage` -- Component providing storage and retrival for settings.
  - `RegistryStorage` -- Windows Registry storage implementation.
- `ISettingsProtect` -- Component that protects secrets.
  - `ProtectImpl` -- Windows DAPI protect implementation.
- `GitLabOptionsPackage` -- Provides integration into VS Options UX.

### VS code suggestions integration

Code suggestions are displayed inline to the user using a public API that is not well documented: 
[`Microsoft.VisualStudio.Language.Proposals`](https://learn.microsoft.com/en-us/dotnet/api/microsoft.visualstudio.language.proposals?view=visualstudiosdk-2022)

See [How Proposals Work](how-proposals-work.md) for more information.

### Code suggestions

Code suggestions are provided through a code suggestions language server. Visual Studio IDE
doesn't support directly using a language server to provide just intellisense (code completions).
Instead the extension integrates into Visual Studio via the `Microsoft.VisualStudio.Language.Proposals`
to provide inline suggestions, and acts as a client to a language server to request suggestions.

The VS integration code is in the `GitLab.Extension/CodeSuggestions` folder, while the language
server client code is in the `GitLab.Extension/LanguageServer` folder. The language server
protocol is STDIO (STDIN/STDOUT).

The language server client performs the following:

1. Manages the language server process (start, stop)
1. Implements required Language Server Protocol messages

#### Components

- `CodeSuggestions.`
  - `GitlabProposalManagerProvider` -- Creates an instance of `GitlabProposalManager`
  - `GitlabProposalManager` -- Tells VS if a proposal should be requested.
  - `GitlabProposalSourceProvider` -- Creates an instance of `GitlabProposalSource`
  - `GitlabProposalSource` -- One per open document, handles document events, requests suggestions.
  - `LanguageManager` -- Defines supported languages mapping content types and file extensions.
  - `Language` -- A supported language
- `LangaugeServer.`
  - `ChildProcessTracker` -- Uses Windows features to link a child process to a parent process. Makes sure we don't leave language server processes behind when Visual Studio is killed.
  - `ExponentialBackoffHelper` -- Exponential backoff implementation. Prevent us from restarting the language server to fast.
  - `ILsClient` -- Client for language server. Allows sending messages. Manages process state.
    - `LsClientBase` -- Common base for `ILsClient` implementations. Most LSP messages implemented here.
    - `LsClientTs` -- TypeScript Language Server client
    - `LsClientGolang` -- Deprecated Go Language Server client
  - `ILsClientManager` -- Factory for getting an `ILsClient` for a specific solution.
    - `LsClientMangager` -- Implementation
  - `IProcessManager` -- Process management for Language Servers
    - `LsProcessManagerTs` -- TypeScript language server implementation
    - `LsProcessManagerGolang` -- Deprecated Go language server implementation
  - `LsClientRpc` -- Server -> Client message handler.
  - `VSVersion` -- Get the version information about visual studio for telemetry

### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. A button that can quickly disable/enable code suggestions
1. Display a code suggestion in progress icon
1. Display an error icon and provide an error message as tool tip.
1. Before the extension has been configured, the error icon will be shown with a message about configuration.

The UX assets are located in the `GitLab.Extension/Resources` folder as PNG files.

The main interactions are between `CodeSuggestions.GitlabProposalSource` and `Status.StatusBar`.

The Visual Studio UX is all written in WPF, a .NET UI library. To display our icon, which
is not officially supported, we locate the status bar in the WPF UI hierarchy and insert a custom
component `CodeSuggestionsStatusControl` which displayes the icon as a button.

### Info Bar

The Info Bar component enables the extension to communicate with the user without
being overly intrusive. The `InfoBar` series of interfaces (`IInfoBar`, `IInfoBarFactory`)
expose a simplified API for creating and managing InfoBars.

Info Bars are initialized and instantiated through the `IInfoBarFactory`, with a helpful
extension method to attach the info bar directly to the main window.

Key features:

1. **Message display**: Shows notifications to the user.
1. **Action integration**: The Info Bar can embed hyperlinks, enabling users to perform
   specific tasks, like navigating to a setting or opening a document.
1. **Custom icons**: Developers can configure the icon displayed next to the message.
1. **Lifetime management**: Developers can programmatically close an open Info Bar.

### Diagrams

Following diagram shows the interaction points between components.

#### Open Solution in Visual Studio

```mermaid
sequenceDiagram

    actor U as User
    participant VS as Visual Studio
    participant Ext as Extension
    participant LS as Language Server

    U->>+VS: Open Solution
    VS->>Ext: VisualStudioPackage.ctor()
    VS->>+Ext: VisualStudioPackage.HandleAfterOpenSolution()
    Ext->>Ext: Logging.ConfigureLogging()
    Ext->>Ext: _statusBar.InitializeDisplay()
    Ext->>Ext: _lsClientManager.GetClient()
    Ext->>Ext: lsClient.ConnectAsync()
    Ext->>LS: Start Process
    Ext->>LS: 'initialize'
    Ext->>LS: 'workspace/didChangeConfiguration'
    deactivate Ext
    deactivate VS
```

#### Open Document and Get Suggestion

```mermaid
sequenceDiagram

    actor U as User
    participant VS as Visual Studio
    participant Ext as Extension
    participant LS as Language Server

    U->>+VS: Open Document
    VS->>Ext: GitlabProposalManagerProvider.ctor()
    Ext->>Ext: GitlabProposalManagerProvider.GetProposalManagerAsync()
    Ext->>Ext: GitlabProposalManager.ctor()
    VS->>Ext: GitLabProposalSourceProvider.ctor()
    VS->>+Ext: GitlabProposalSourceProvider.GetProposalSourceAsync()
    Ext->>Ext: GitlabProposalSource.ctor()
    deactivate Ext

    VS->>+Ext: Event TextBuffer_OpenedAsync()
    Ext->>Ext: LsClient.SendTextDocumentDidOpenAsync()
    Ext->>LS: 'textDocument/didOpen'
    deactivate Ext
    deactivate VS

    U->>+VS: Type character
    VS->>+Ext: Event TextBuffer_ChangedAsync()
    Ext->>Ext: LsClient.SendTextDocumentDidChangeAsync()
    Ext->>LS: 'textDocument/didChange'
    deactivate Ext

    VS->>+Ext: GitlabProposalManager.TryGetIsProposalPossition()
    VS->>Ext: GitlabProposalSource.RequestProposalAsync()
    Ext->>Ext: LsClient.SendTextDocumentCompletionAsync()
    Ext->>LS: 'textDocument/completion'
    LS-->>Ext: Completion from GitLab
    Ext-->>VS: Proposal's
    VS->>Ext: GitlabProposalSource.CommitSuggestion()
    deactivate Ext
    deactivate VS

    U->>+VS: Close document
    VS->>+Ext: Event TextView_Closed
    Ext->>Ext: LsClient.SendTextDocumentCloseAsync()
    Ext->>LS: 'textDocument/didClose'
    deactivate Ext
    deactivate VS
```

#### Settings Changed

```mermaid
sequenceDiagram

    actor U as User
    participant VS as Visual Studio
    participant Ext as Extension
    participant LS as Language Server

    U->>+VS: Open VS Options
    VS->>Ext: GitLabOptionsPackage.ctor()
    VS->>Ext: Show OptionPageGrid
    deactivate VS
    U->>+VS: Edit options
    VS->>+Ext: LsClientTs.SettingsChangedEvent
    Ext->>LS: 'workspace/didChangeConfiguration'
    deactivate Ext
    deactivate VS
```

#### Close Solution

```mermaid
sequenceDiagram

    actor U as User
    participant VS as Visual Studio
    participant Ext as Extension
    participant LS as Language Server

    U->>+VS: Close Solution
    VS->>+Ext: VisualStudioPackage.HandleBeforeCloseSolution()
    Ext->>Ext: LsClientManager.DisposeClientAsync()
    Ext->>Ext: LsProcessManager.StopLanguageServerAsync()
    Ext->>LS: Kill Process
    deactivate Ext
    deactivate VS
```

#### Validate GitLab Access Token

```mermaid
sequenceDiagram
    Actor U as User
    Participant VS as Visual Studio
    Participant LS as Language Server
    Participant EXT as Extension
    Participant TokenService as InvalidTokenNotificationService


    U->>+VS: Open Solution
    VS->>+EXT: VisualStudioPackage.ctor()
    EXT->>+TokenService: InvalidTokenNotificationService.ctor()
    TokenService->>TokenService: IObservable<TokenValidationNotification>.Subscribe()

    deactivate VS
    deactivate EXT
    deactivate TokenService

    activate LS
    LS->>+EXT: '$/gitlab/token/check'
    EXT->>EXT: LsClientRpc.gitlab_token_check()
    EXT->>EXT: IObserver<TokenValidationNotification>.OnNext()

    deactivate LS
    deactivate EXT

    activate TokenService
    TokenService->>TokenService: HandleTokenValidationNotification(tokenParams)
    TokenService->>+EXT: IInfoBarFactory.AttachInfoBarToMainWindow(message, actions)

    deactivate TokenService
    deactivate EXT
```

#### Attaching an InfoBar

```mermaid
sequenceDiagram

    participant Ext as Extension
    participant InfoBarFactory as InfoBarFactory
    participant VS as Visual Studio

    participant InfoBar as InfoBar

    Ext->>+InfoBarFactory: AttachInfoBar(host, message, actions)
    InfoBarFactory->>VS: IVsInfoBarUIFactory.CreateInfoBar()
    VS->>InfoBarFactory: IVsInfoBarUIElement
    InfoBarFactory->>VS: host.AddInfoBar()
    InfoBarFactory->>InfoBar: InfoBar.ctor()
    InfoBar->>VS: enable IVsInfoBarUIEvents callbacks
    InfoBarFactory->>Ext: IInfoBar

    deactivate InfoBarFactory

    Ext->>+InfoBarFactory: AttachInfoBarToMainWindow(message, actions)
    InfoBarFactory->>VS: IVsShell.GetProperty()
    VS->>InfoBarFactory: IVsInfoBarHost
    InfoBarFactory->>InfoBarFactory: AttachInfoBar()
    InfoBarFactory->>Ext: IInfoBar

    deactivate InfoBarFactory

```
