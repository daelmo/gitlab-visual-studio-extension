# <img src="https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/raw/18c5fd65285b4c2ae30356e81105854ee7ef5213/GitLab.Extension/gitlab-logo.png" width="64" align="center" /> [GitLab Extension for Visual Studio IDE](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension)

This is the GitLab extension for Visual Studio (Community, Pro, and Enterprise).
It is not the [extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow).

## Minimum supported version

The GitLab for Visual Studio extension supports Code Suggestions for both
[GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas) and
[GitLab self-managed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-self-managed-gitlab).

This extension only supports Visual Studio 2022.

Visual Studio for Mac is not supported.

## Setup

Prerequisites:

- You must be using GitLab Enterprise Edition.
- For self-managed installations, Code Suggestions must be
  [enabled for your instance](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html).
- For GitLab.com, Code Suggestions must be
  enabled for your top-level group. See instructions
  [for SaaS](https://docs.gitlab.com/ee/user/group/manage.html#enable-code-suggestions-for-a-group).

1. [Install the extension](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio) from the Visual Studio Marketplace and enable it.
1. In GitLab, create a [GitLab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` and `read_user` scopes:
1. Copy the token. _For security reasons, this value is never displayed again, so you must copy this value now._
1. Open Visual Studio.
   1. On the top bar, go to **Tools > Options > GitLab**.
   1. In the **Access Token** field, paste in your token. The token is not displayed, nor is it accessible to others.
   1. In the **GitLab URL** field, provide the URL of your GitLab instance. For GitLab SaaS, use `https://gitlab.com`.

## Features

### Code Suggestions

Write code more efficiently by using generative AI to suggest code while you’re developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)

Code Suggestions is a generative artificial intelligence (AI) model.
To learn more, see the GitLab documentation for
[Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/).

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Google Vertex AI Codey APIs Data Governance](https://cloud.google.com/vertex-ai/docs/generative-ai/data-governance)

Users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations)

#### Supported Languages

- C++
- C#
- Go
- Google SQL
- Java
- JavaScript
- Kotlin
- PHP
- Python
- Ruby
- Rust
- Scala
- Swift
- TypeScript

#### Usage

- `Tab` accepts suggestion
- `Escape` dismisses suggestion

#### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. A button that can quickly disable/enable code suggestions.
1. Display a code suggestion in progress icon.
1. Display an error icon and provide an error message as tooltip.
1. Before the extension has been configured, the error icon is shown with a message about configuration.

![status_bar_visual_studio.png](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/raw/main/docs/assets/status_bar_visual_studio.png)

#### Commands

The extension registers custom commands with the Visual Studio IDE. These commands
can be accessed with keyboard shortcuts, which you can personalize:

1. On the top bar, go to **Tools > Options**.
1. Go to **Environment > Keyboard**. Commands exposed by this extension are prefixed with `GitLab.`.
1. Select a command, and assign it a keyboard shortcut.

These commands are available:

| Command name                   | Default keyboard shortcut | Feature |
|--------------------------------|---------------------------|---------|
| `GitLab.ToggleCodeSuggestions` | N/A                       | Enable or disable automated code suggestions. |

## Troubleshooting

See the [troubleshooting documentation](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/docs/user/troubleshooting.md).

Report issues in the
[feedback issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/38).

## Feedback

We'd love to hear from you. If you've found a bug, or have an idea,
[open an issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/new).

## Contributing

This extension is open source and [hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension). Contributions are more than welcome and subject to the terms set forth in [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md). Feel free to fork and add new features or submit bug reports. See [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md) for more information.
