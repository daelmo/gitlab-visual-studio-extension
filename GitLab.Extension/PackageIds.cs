﻿using System;

namespace GitLab.Extension
{
    public static class PackageIds
    {
        public const string GitLabPackageIdString = "4c68a5e8-056b-4fe9-b381-50208f5e8f35";
        public static readonly Guid GitLabPackage = new Guid(GitLabPackageIdString);
        public static readonly Guid GitLabPackageCmdSet = new Guid("{114BEDCA-3334-4577-A410-16D264708C6A}");
        public static readonly int GL_MENU_DEFAULT = 0x1010;
        public static readonly int GL_GROUP_TOOLS = 0x1000;
        public static readonly int GL_GROUP_DEFAULT = 0x1011;
        public static readonly int GL_CMD_TOGGLE_CODE_SUGGESTIONS = 0x2002;
    }
}
