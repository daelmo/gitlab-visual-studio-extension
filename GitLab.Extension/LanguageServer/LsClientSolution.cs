﻿namespace GitLab.Extension.LanguageServer
{
    public struct LsClientSolution
    {
        public string Name;
        public string Path;

        public LsClientSolution(string name, string path)
        {
            Name = name;
            Path = path;
        }
    }
}
