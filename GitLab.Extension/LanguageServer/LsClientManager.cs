﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Manage instances of LsClient
    /// </summary>
    public class LsClientManager : ILsClientManager
    {
        private readonly Dictionary<string, ILsClient> _clients = new Dictionary<string, ILsClient>();
        private readonly object _clientsLock = new object();
        private readonly Func<LsClientSolution, ILsClient> _lsClientGenerator;

        public LsClientManager(Func<LsClientSolution, ILsClient> lsClientFactory)
        {
            _lsClientGenerator = lsClientFactory;
        }

        ~LsClientManager()
        {
            foreach(var client in _clients.Values)
                client.Dispose();

            _clients.Clear();
        }

        /// <summary>
        /// Get an LsClient instance for a solution. A single LsClient instance
        /// is shared for eash unique solutionName.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        public ILsClient GetClient(string solutionName, string solutionPath)
        {
            lock (_clientsLock)
            {
                if (!_clients.TryGetValue(solutionName, out ILsClient client))
                {
                    client = _lsClientGenerator(new LsClientSolution(solutionName, solutionPath));
                    _clients[solutionName] = client;
                }

                return client;
            }
        }

        /// <summary>
        /// Dispose an LsClient. This is only called by LsVisualStudioPackage.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <returns></returns>
        public async Task DisposeClientAsync(string solutionName)
        {
            ILsClient client = null;
            lock (_clientsLock)
            {

                if (!_clients.TryGetValue(solutionName, out client))
                    return;

                _clients.Remove(solutionName);
            }

            await client.DisposeAsync();
        }
    }
}
