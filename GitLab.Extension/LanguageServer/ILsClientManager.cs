﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    public interface ILsClientManager
    {
        /// <summary>
        /// Get an LsClient instance for a solution. A single LsClient instance
        /// is shared for eash unique solutionName.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        ILsClient GetClient(string solutionName, string solutionPath);

        /// <summary>
        /// Dispose an LsClient. This is only called by LsVisualStudioPackage.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <returns></returns>
        Task DisposeClientAsync(string solutionName);
    }
}
