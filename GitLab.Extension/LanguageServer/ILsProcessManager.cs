﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    public interface ILsProcessManager : IAsyncDisposable
    {
        /// <summary>
        /// Language server executable file
        /// </summary>
        string LanguageServerExecutable { get; }

        /// <summary>
        /// Start a language server watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabUrl"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="port">Port the server is listening on</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        bool StartLanguageServer(string solutionPath, string gitlabUrl, string gitlabToken, out int port);

        /// <summary>
        /// Start a language server using stdio watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabUrl"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="stdin">Input stream</param>
        /// <param name="stdout">Output stream</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        bool StartLanguageServerStdio(string solutionPath, string gitlabUrl, string gitlabToken, out Stream stdin, out Stream stdout);

        /// <summary>
        /// Stop the language server associated with solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true if a server was killed, false otherwise.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        Task<bool> StopLanguageServerAsync(string solutionPath);
    }
}
