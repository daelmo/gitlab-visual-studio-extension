﻿using System;

namespace GitLab.Extension.LanguageServer
{
    public static class LsGlobalEvents
    {
        public static event EventHandler TokenInvalidEvent;

        internal static void OnTokenInvalidEvent(object sender)
        {
            TokenInvalidEvent.Invoke(sender, null);
        }
    }
}
