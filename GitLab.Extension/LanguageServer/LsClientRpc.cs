﻿using GitLab.Extension.LanguageServer.Models;
using Serilog;
using StreamJsonRpc;
using System;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Handlers for LSP messages sent from the language server to the client.
    /// </summary>
    public class LsClientRpc
    {
        private readonly IObserver<TokenValidationNotification> _tokenValidationObserver;

        public LsClientRpc(
            IObserver<TokenValidationNotification> tokenValidationObserver)
        {
            _tokenValidationObserver = tokenValidationObserver;
        }

        [JsonRpcMethod("textDocument/publishDiagnostics")]
        public void textDocument_publishDiagnostics(dynamic pdParams)
        {
            // The language server reports errors this way.
            // Right now we don't have a way to handle errors
            // outside of logging it to the debug view.
            Log.Debug("textDocument/publishDiagnostics: {Diagnostic}",
                pdParams.Diagnostic);
        }

        [JsonRpcMethod("$/gitlab/token/check", 
            UseSingleObjectParameterDeserialization = true)]
        public void gitlab_token_check(
            TokenValidationNotification tokenParams)
        {
            Log.Debug("$/gitlab/token/check: {Reason} {Message}", tokenParams.reason, tokenParams.message);

            _tokenValidationObserver.OnNext(tokenParams);
        }
    }
}
