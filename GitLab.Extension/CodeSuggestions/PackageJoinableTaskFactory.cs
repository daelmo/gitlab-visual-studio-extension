﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Threading;
using System.ComponentModel.Composition;

namespace GitLab.Extension.CodeSuggestions
{
    internal class PackageJoinableTaskFactoryWrapper : IDisposable
    {
        [Export("GitLab.Extension.CodeSuggestions.PackageJoinableTaskCollection")]
        private JoinableTaskCollection _joinableTaskCollection;
        [Export("GitLab.Extension.CodeSuggestions.PackageJoinableTaskFactory")]
        private readonly JoinableTaskFactory _joinableTaskFactory;

        [ImportingConstructor]
        public PackageJoinableTaskFactoryWrapper(JoinableTaskContext joinableTaskContext)
        {
            _joinableTaskCollection = joinableTaskContext.CreateCollection();
            _joinableTaskCollection.DisplayName = "GitLab.Extension.CodeSuggestions";
            _joinableTaskFactory = joinableTaskContext.CreateFactory(_joinableTaskCollection);
        }

        public void Dispose()
        {
            ThreadHelper.ThrowIfNotOnUIThread(nameof(Dispose));
            try
            {
                ThreadHelper.JoinableTaskFactory.Run(
                    new Func<Task>(_joinableTaskCollection.JoinTillEmptyAsync));
            }
            catch (OperationCanceledException)
            {
                // Always ignore operation canceled exceptions
            }
            catch (AggregateException)
            {
                // Ignore exceptions during dispose.
            }

            GC.KeepAlive(this);
        }
    }
}
