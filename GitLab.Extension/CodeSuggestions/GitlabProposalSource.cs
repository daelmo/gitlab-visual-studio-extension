﻿using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.SettingsUtil;
using Serilog;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Workspace;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Generate inline proposals for a unique IWpfTextView instance.
    /// Each open file will have it's own GitlabProposalSource instance.
    /// When the file is closed, the associated GitlabProposalSource instance will be disposed.
    /// </summary>
    public class GitlabProposalSource : ProposalSourceBase
    {
        public const string SourceName = nameof(GitlabProposalSource);
        private readonly IWpfTextView _textView;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ISettings _settings;
        private readonly Status.StatusBar _statusBar;
        private readonly ILogger _logger;

        private bool _disposed = false;
        private string _fullFilePath;
        private ILsClient _lsClient;
        private string _relativeFilePath;
        private WorkspaceId _workspaceId;

        public GitlabProposalSource(
            IWorkspaceLsClientProvider lsClientProvider,
            Status.StatusBar statusBar, 
            ISettings settings, 
            IWpfTextView textView, 
            ILogger logger)
        {
            _logger = logger;
            _lsClientProvider = lsClientProvider;

            _logger.Debug($"{nameof(GitlabProposalSource)}");
            
            _textView = textView;
            _settings = settings;
            _statusBar = statusBar;

            var uiElement = _textView as UIElement;

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            uiElement.Dispatcher.Invoke(new Action(() =>
            {
                ThreadHelper.ThrowIfNotOnUIThread();

                _workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
                

                _fullFilePath = GetTextViewFilePath();
                if (_fullFilePath.StartsWith(_workspaceId.SolutionPath))
                    _relativeFilePath = _fullFilePath.Substring(_workspaceId.SolutionPath.Length + 1);
                else
                    _relativeFilePath = _fullFilePath;
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs

            _textView.TextBuffer.Changed += TextBuffer_ChangedAsync;
            _textView.Closed += textView_Closed;
            _logger = logger;
        }

        private void textView_Closed(object sender, EventArgs e)
        {
            if (!_settings.IsCodeSuggestionsEnabled)
                return;

            if (_lsClient == null)
                return;

            _ = _lsClient.SendTextDocumentDidCloseAsync(_relativeFilePath);
        }

        public async Task<bool> StartLanguageServerClientAsync()
        {
            try
            {
                _logger.Debug($"{nameof(StartLanguageServerClientAsync)}");

                if (!_settings.Configured)
                {
                    _logger.Debug($"{nameof(StartLanguageServerClientAsync)}: Settings not configured, not starting a language server client");
                    _statusBar.CodeSuggestionsError("Extension not configured.", true);
                    return false;
                }

                if (!_settings.IsCodeSuggestionsEnabled)
                {
                    _logger.Debug($"{nameof(StartLanguageServerClientAsync)}: IsCodeSuggestionEnabled == false, not starting a language server client");
                    return false;
                }

                if (_lsClient != null)
                    await _lsClient.DisposeAsync();
                
                _lsClient = _lsClientProvider.GetClient(_workspaceId);
                await _lsClient.ConnectAsync();
                await TextBuffer_OpenedAsync();

                _statusBar.CodeSuggestionsClearError();

                return true;
            }
            catch(Exception ex)
            {
                _logger.Debug(ex, $"{nameof(StartLanguageServerClientAsync)}: Exception");
                _statusBar.CodeSuggestionsError("Exception occured starting the language server. If this error continues, please reach out to support.");
                return false;
            }
        }

        /// <summary>
        /// Get the filename we are editing
        /// </summary>
        /// <returns></returns>
        private string GetTextViewFilePath()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            _textView.TextBuffer.Properties.TryGetProperty(typeof(IVsTextBuffer), out IVsTextBuffer bufferAdapter);
            var persistFileFormat = bufferAdapter as IPersistFileFormat;

            if (persistFileFormat == null)
                return null;

            persistFileFormat.GetCurFile(out var filepath, out _);

            if (string.IsNullOrEmpty(filepath))
                return null;

            return filepath;
        }

        private async Task TextBuffer_OpenedAsync()
        {
            if (!_settings.IsCodeSuggestionsEnabled)
                return;

            if (_lsClient == null && !await StartLanguageServerClientAsync())
                return;

            await _lsClient.SendTextDocumentDidOpenAsync(
                _relativeFilePath,
                _textView.TextBuffer.CurrentSnapshot.Version.VersionNumber,
                _textView.TextBuffer.CurrentSnapshot.GetText());
        }

#pragma warning disable VSTHRD100
#pragma warning disable VSTHRD200
        /// <summary>
        /// Send changes to language server.
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TextBuffer_ChangedAsync(object sender, TextContentChangedEventArgs e)
        {
            try
            {
                _logger.Debug($"{nameof(TextBuffer_ChangedAsync)}");

                if (!_settings.IsCodeSuggestionsEnabled)
                {
                    _logger.Debug($"{nameof(TextBuffer_ChangedAsync)} !_settings.IsCodeSuggestionsEnabled, returning");
                    return;
                }

                if (_lsClient == null && !await StartLanguageServerClientAsync())
                {
                    _logger.Debug($"{nameof(TextBuffer_ChangedAsync)} Failed to start an LsClient, returning");
                    return;
                }

                switch(_lsClient.TextDocumentSyncKind)
                {
                    case LanguageServer.Models.TextDocumentSyncKind.Full:
                        await SendChangedFullAsync(sender, e);
                        return;
                    case LanguageServer.Models.TextDocumentSyncKind.Incremental:
                        await SendChangedIncrementalAsync(sender, e);
                        return;
                    case TextDocumentSyncKind.None:
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Debug(ex, $"{nameof(TextBuffer_ChangedAsync)} Exception");
            }
        }

        /// <summary>
        /// Send changes to language server using the Incremental sync method
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task SendChangedIncrementalAsync(object sender, TextContentChangedEventArgs e)
        {
            try
            {
                _logger.Debug($"{nameof(SendChangedIncrementalAsync)}");

                var versionNumber = e.AfterVersion.VersionNumber;

                var changes = new TextDocumentContentChangeEvent[e.Changes.Count];

                for (int changeIndex = 0; changeIndex < e.Changes.Count; changeIndex++)
                {
                    var oldStartPosition = e.Changes[changeIndex].OldSpan.Start;
                    var oldEndPosition = e.Changes[changeIndex].OldSpan.End;
                    var changedText = e.Changes[changeIndex].NewText;

                    var start = PositionToLineAndCharacter(e.Before, oldStartPosition);
                    var end = PositionToLineAndCharacter(e.Before, oldEndPosition);

                    changes[changeIndex] = new TextDocumentContentChangeEvent
                    {
                        range = new Range
                        {
                            start = new Position
                            {
                                line = (uint)start.line,
                                character = (uint)start.character,
                            },
                            end = new Position
                            {
                                line = (uint)end.line,
                                character = (uint)end.character,
                            }
                        },
                        text = changedText,
                    };
                }

                await _lsClient.SendTextDocumentDidChangeAsync(
                    _relativeFilePath, versionNumber, changes);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Debug(ex, $"{nameof(SendChangedIncrementalAsync)} Exception");
            }
        }

        private (int line, int character) PositionToLineAndCharacter(ITextSnapshot snapshot, int position)
        {
            //if (position == 0)
            //    return (0, 0);

            var line = snapshot.GetLineFromPosition(position);
            var lineNumber = line.LineNumber;
            var character = position - line.Start;

            return (lineNumber, character);
        }

        /// <summary>
        /// Send changes to language server using the Full sync method
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task SendChangedFullAsync(object sender, TextContentChangedEventArgs e)
        {
            try
            {
                _logger.Debug($"{nameof(SendChangedFullAsync)}");

                var changedText = e.After.GetText();
                var versionNumber = e.AfterVersion.VersionNumber;
                var fullText = e.AfterVersion.TextBuffer.CurrentSnapshot.GetText();

                await _lsClient.SendTextDocumentDidChangeAsync(
                    _relativeFilePath,
                    versionNumber,
                    fullText);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Debug(ex, $"{nameof(SendChangedFullAsync)} Exception");
            }
        }
#pragma warning restore VSTHRD100
#pragma warning restore VSTHRD200

        /// <summary>
        /// Called by Visual Studio to get a list of proposals from our extension. This
        /// triggers a call to get a code suggestion.
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="scenario"></param>
        /// <param name="triggeringCharacter"></param>
        /// <param name="token"></param>
        /// <returns>Returns a GitlabProposalCollection instance or null on error.</returns>
        public override async Task<ProposalCollectionBase> RequestProposalsAsync(
            VirtualSnapshotPoint caret, 
            CompletionState completionState, 
            ProposalScenario scenario, 
            char triggeringCharacter, 
            CancellationToken token)
        {
            // Return null to avoid the possiblity of Visual Studio 
            // seeing an exception as a bug and reporting it to the user.
            if (_disposed)
                return null;

            // We can't provide code suggestions until the extension is configured
            if (!_settings.Configured)
                return null;

            // Don't request a suggestion if code suggestions are disabled
            if (!_settings.IsCodeSuggestionsEnabled)
                return null;

            var error = null as string;

            try
            {
                _statusBar.CodeSuggestionsInProgressStart();

                // If the extension was just configured, we will need
                // to start the language server.
                if (_lsClient == null && !await StartLanguageServerClientAsync())
                        return null;

                // Wait 150 milliseconds in case the user is typing quickly.
                token.WaitHandle.WaitOne(150);
                if (token.IsCancellationRequested)
                    return null;

                // Get code suggestion from language service
                var ret = await GetCodeSuggestionAsync(token);
                error = ret.error;

                if(ret.completion == null || error != null)
                {
                    return null;
                }

                if (string.IsNullOrWhiteSpace(ret.completion.insertText))
                {
                    _ = _lsClient.SendGitlabTelemetryCodeSuggestionNotProvidedAsync(ret.completion.data.trackingId);
                    return null;
                }

                if(token.IsCancellationRequested)
                {
                    _ = _lsClient.SendGitlabTelemetryCodeSuggestionCancelledAsync(ret.completion.data.trackingId);
                    return null;
                }
                
                return CreateProposalCollection(caret, completionState, ret.completion);
            }
            finally
            {
                _statusBar.CodeSuggestionsInProgressComplete();
                if (error != null)
                    _statusBar.CodeSuggestionsError(error);
            }
        }

        /// <summary>
        /// Called when a code suggestion has been accepted to commit the change to the buffer.
        /// </summary>
        /// <param name="suggestion"></param>
        private void CommitSuggestion(string suggestion)
        {
            _textView.TextBuffer.Insert(_textView.Caret.Position.BufferPosition, suggestion);
        }

        /// <summary>
        /// Create a GitlabProposalCollection from a code suggestion
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="suggestion"></param>
        /// <returns></returns>
        private ProposalCollection CreateProposalCollection(
            VirtualSnapshotPoint caret,
            CompletionState completionState, 
            CompletionItem suggestion)
        {
            // Create proposals
            const ProposalFlags flags =
                ProposalFlags.FormatAfterCommit |
                ProposalFlags.MoveCaretToEnd |
                ProposalFlags.DisableInIntelliSense |
                ProposalFlags.SimulateBraceCompletion;
            
            var displaySuggestion = suggestion.insertText;

            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var column = bufferPosition.Position - line.Start.Position;

            // If the caret is in virtual space and the column position is zero,
            // it will look to the user like the caret is indented even through
            // the column is zero. Our code suggestion be based on a zero column
            // position and include the needed indentation.
            //
            // To make the display of the suggestion look correct to the user
            // we need to trim any preceding indentation. But only for the display
            // value. We want to insert with the correct indentation.
            if (_textView.Caret.InVirtualSpace && column == 0)
                displaySuggestion = displaySuggestion.TrimStart();

            var edits = new List<ProposedEdit>
            {
                new ProposedEdit(
                    _textView.GetTextElementSpan(_textView.Caret.Position.BufferPosition),
                    displaySuggestion)
            };

            var proposalId = "gitlab:" + suggestion.data.trackingId;
            var proposalMetadata =
                new GitLabProposalMetadata(
                    _workspaceId,
                    suggestion.data.trackingId);

            var proposals = new List<ProposalBase>
            {
                GitLabProposal.TryCreateProposal(
                    proposalMetadata,
                    null,
                    edits,
                    caret,
                    completionState,
                    flags,
                    proposalId: proposalId)
            };

            return new ProposalCollection(SourceName, proposals);
        }

        /// <summary>
        /// Call the Code Suggestion API and return a suggestion.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Returns a suggestion or null</returns>
        private async Task<(CompletionItem completion, string error)> GetCodeSuggestionAsync(CancellationToken cancellationToken)
        {
            _logger.Debug($"{nameof(GetCodeSuggestionAsync)}");

            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var lineNumber = line.LineNumber;
            var column = bufferPosition.Position - line.Start.Position;

            if (cancellationToken.IsCancellationRequested)
            {
                _logger.Debug($"{nameof(GetCodeSuggestionAsync)} CancellationRequested, Returning (null, null)");
                return (null, null);
            }

            var ret = await _lsClient.SendTextDocumentCompletionAsync(
                _relativeFilePath,
                (uint)lineNumber,
                (uint)column,
                cancellationToken);

            if (ret.Completions == null || ret.Completions.Length == 0)
            {
                _logger.Debug($"{nameof(GetCodeSuggestionAsync)} Returning (null, {{Error}})",
                    ret.Error);
                return (null, ret.Error);
            }

            return (ret.Completions[0], ret.Error);
        }

#pragma warning disable CS1998
        public override async Task DisposeAsync()
        {
            if (_disposed)
                return;

            _lsClient = null;
            _disposed = true;
        }
#pragma warning restore CS1998
    }
}
