using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions.Model;

namespace GitLab.Extension.CodeSuggestions
{
    public interface IGitLabCodeSuggestionTelemetryController
    {
        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is shown to the user.
        /// </summary>
        /// <param name="proposalMetadata">The GitLab proposal that was shown.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnShownAsync(
            GitLabProposalMetadata proposalMetadata);

        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is rejected by the user.
        /// </summary>
        /// <param name="proposalMetadata">The GitLab proposal that was rejected.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnRejectedAsync(
            GitLabProposalMetadata proposalMetadata);

        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is accepted by the user.
        /// </summary>
        /// <param name="proposalMetadata">The GitLab proposal that was rejected.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnAcceptedAsync(
            GitLabProposalMetadata proposalMetadata);
        
        /// <summary>
        /// Called when a suggestion is dismissed by a user, but does not contain proposal info
        /// </summary>
        /// <returns>A task representing the asynchronous operation</returns>
        Task RejectAllManagedProposalsAsync();
    }
}
