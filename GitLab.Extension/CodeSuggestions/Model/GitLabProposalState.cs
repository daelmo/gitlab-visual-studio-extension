namespace GitLab.Extension.CodeSuggestions.Model
{
    internal enum GitLabProposalState
    {
        Shown,
        Rejected,
        Accepted
    }
}
