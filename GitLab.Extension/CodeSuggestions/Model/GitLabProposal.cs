using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text;

namespace GitLab.Extension.CodeSuggestions.Model
{
    /// <summary>
    /// Represents a GitLab code suggestion proposal, extending the base Proposal class.
    /// This class encapsulates the details specific to GitLab proposals, such as telemetry and workspace ids,
    /// while leveraging the underlying Proposal data model.
    /// </summary>
    public class GitLabProposal : Proposal
    {
        public GitLabProposalMetadata GitLabProposalMetadata { get; }

        public static GitLabProposal TryCreateProposal(
            GitLabProposalMetadata proposalMetadata,
            string description,
            IReadOnlyList<ProposedEdit> edits,
            VirtualSnapshotPoint caret,
            CompletionState completionState = null,
            ProposalFlags flags = ProposalFlags.None,
            Func<bool> commitAction = null,
            string proposalId = null,
            string acceptText = null,
            string nextText = null,
            NormalizedSnapshotSpanCollection scope = null)
        {
            return ValidateEdits(edits, caret, completionState)
                ? new GitLabProposal(
                    proposalMetadata, 
                    description, 
                    edits, 
                    caret, 
                    completionState, 
                    flags, 
                    commitAction, 
                    proposalId,
                    acceptText, 
                    nextText, 
                    scope)
                : null;
        }

        public GitLabProposal(
            GitLabProposalMetadata proposalMetadata,
            string description,
            IReadOnlyList<ProposedEdit> edits,
            VirtualSnapshotPoint caret,
            CompletionState completionState = null,
            ProposalFlags flags = ProposalFlags.None,
            Func<bool> commitAction = null,
            string proposalId = null,
            string acceptText = null,
            string nextText = null,
            NormalizedSnapshotSpanCollection scope = null)
            : base(description, edits, caret, completionState, flags, commitAction, proposalId, acceptText, nextText)
        {
            GitLabProposalMetadata = proposalMetadata;
        }
    }
}
