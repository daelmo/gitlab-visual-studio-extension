using System;
using System.Linq;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Language.Suggestions;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.CodeSuggestions
{
    public class SuggestionServiceTelemetryAdaptor : IDisposable
    {
        private readonly SuggestionServiceBase _suggestionServiceBase;
        private readonly IGitLabCodeSuggestionTelemetryController _telemetryController;

        private static readonly string[] _knownProviderNames =
            {
                "IntelliCodeLineCompletions",
                GitlabProposalSource.SourceName
            };
        
        public SuggestionServiceTelemetryAdaptor(
            SuggestionServiceBase suggestionServiceBase,
            IGitLabCodeSuggestionTelemetryController telemetryController)
        {
            _suggestionServiceBase = suggestionServiceBase;
            _telemetryController = telemetryController;

            _suggestionServiceBase.ProposalDisplayed += HandleProposalDisplayed;
            _suggestionServiceBase.ProposalRejected += HandleProposalRejected;
            _suggestionServiceBase.SuggestionAccepted += HandleSuggestionAccepted;
            _suggestionServiceBase.SuggestionDismissed += HandleSuggestionDismissed;
        }
        
        private void HandleProposalDisplayed(
            object sender,
            ProposalDisplayedEventArgs args)
        {
            HandleTelemetryEvent(
                args.ProviderName,
                args.OriginalProposal,
                GitLabProposalState.Shown);
        }

        private void HandleProposalRejected(
            object sender,
            ProposalRejectedEventArgs args)
        {
            HandleTelemetryEvent(
                args.ProviderName,
                args.OriginalProposal,
                GitLabProposalState.Rejected);
        }

        private void HandleSuggestionAccepted(
            object sender,
            SuggestionAcceptedEventArgs args)
        {
            HandleTelemetryEvent(
                args.ProviderName,
                args.OriginalProposal,
                GitLabProposalState.Accepted);
        }

        private void HandleSuggestionDismissed(
            object sender,
            SuggestionDismissedEventArgs args)
        {
            if (args.Reason == ReasonForDismiss.DismissedAfterBackspace)
            {
                // MS doesn't reject proposals in cases of backspace, we'll need to manually reject all managed proposals
                ThreadHelper.JoinableTaskFactory
                    .RunAsync(() => _telemetryController.RejectAllManagedProposalsAsync())
                    .FileAndForget("GitLabCodeSuggestionTelemetryService.HandleSuggestionDismissed.DismissedAfterBackspace");

                return;
            }
            
            HandleTelemetryEvent(
                args.ProviderName,
                args.OriginalProposal,
                GitLabProposalState.Rejected);
        }

        private void HandleTelemetryEvent(
            string providerName,
            ProposalBase originalProposal,
            GitLabProposalState action)
        {
            if (!_knownProviderNames.Contains(providerName) || !(originalProposal is GitLabProposal proposal))
                return;
            
            ThreadHelper.JoinableTaskFactory
                .RunAsync(() =>
                {
                    switch (action)
                    {
                        case GitLabProposalState.Shown:
                            return _telemetryController.OnShownAsync(proposal.GitLabProposalMetadata);
                        case GitLabProposalState.Rejected:
                            return _telemetryController.OnRejectedAsync(proposal.GitLabProposalMetadata);
                        case GitLabProposalState.Accepted:
                            return _telemetryController.OnAcceptedAsync(proposal.GitLabProposalMetadata);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(action), action, null);
                    }
                })
                .FileAndForget($"GitLabCodeSuggestionTelemetryService.HandleTelemetryAction_{action}");
        }

        public void Dispose()
        {
            _suggestionServiceBase.ProposalDisplayed -= HandleProposalDisplayed;
            _suggestionServiceBase.ProposalRejected -= HandleProposalRejected;
            _suggestionServiceBase.SuggestionAccepted -= HandleSuggestionAccepted;
            _suggestionServiceBase.SuggestionDismissed -= HandleSuggestionDismissed;
        }
    }
}
