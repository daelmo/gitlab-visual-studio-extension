﻿using Microsoft.VisualStudio.Shell.Interop;
using System;

namespace GitLab.Extension.SettingsUtil
{
    public static class GitLabOptionsPackageActions
    {
        public static void ShowOptionsPage()
        {
            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();

            var shell = (IVsShell)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SVsShell));

            var packageGuid = new Guid(GitLabOptionsPackage.PackageGuidString);

            shell.IsPackageLoaded(ref packageGuid, out var vsPackage);
            if (vsPackage == null) 
            { 
                shell.LoadPackage(ref packageGuid, out vsPackage);
            }

            if (vsPackage is Microsoft.VisualStudio.Shell.Package package)
            {
                package.ShowOptionPage(typeof(OptionPageGrid));
            }
        }
    }
}
