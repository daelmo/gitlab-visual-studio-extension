﻿using System;
using System.Reactive.Linq;
using GitLab.Extension.LanguageServer.Models;
using Serilog.Events;

namespace GitLab.Extension.SettingsUtil
{
    /// <summary>
    /// Extension settings. Setting are stored in the registry.
    /// </summary>
    public class Settings : ISettings, IDisposable
    {
        public class SettingsEventArgs : EventArgs
        {
            public string ChangedSettingKey;

            public SettingsEventArgs(string changedSettingKey)
            {
                ChangedSettingKey = changedSettingKey;
            }
        }

        /// <summary>
        /// Old registry key from PoC extension
        /// </summary>
        public const string ApplicationNamePoC = "GitLabCodeSuggestionsVisualStudio";
        /// <summary>
        /// Registry key name for settings
        /// </summary>
        public const string ApplicationName = "GitLabExtensionVisualStudio";
        public const string GitLabAccessTokenKey = "GitLabAccessToken";
        public const string IsCodeSuggestionsEnabledKey = "IsCodeSuggestionsEnabled";
        public const string IsTelemetryEnabledKey = "IsTelemetryEnabled";
        public const string GitLabUrlKey = "GitLabUrl";
        public const string LogLevelKey = "LogLevel";

        private bool _batchSettingsUpdate = false;
        private bool _isGitLabAccessTokenValid = true;
        private IDisposable _tokenValidationNotificationsSubscription;

        public ISettingsStorage Storage = null;
        public string GitlabAccessTokenValue = null;
        public bool IsCodeSuggestionsEnabledValue = true;
        public bool IsTelemetryEnabledValue = true;
        public string GitLabUrlValue = null;
        public LogEventLevel LogLevelValue = LogEventLevel.Warning;

        public event EventHandler SettingsChangedEvent;

        public Settings(
            ISettingsStorage storage,
            IObservable<TokenValidationNotification> tokenValidationNotifications)
        {
            try
            {
                Storage = storage;
                Storage.Load(this);
            }
            catch
            {
                // Ignore any exceptions from initial loading.
                // The storage componet will already have logged
                // the loading error.
            }

            _tokenValidationNotificationsSubscription = 
                tokenValidationNotifications.Subscribe(
                    HandleTokenValidationNotification);
        }

        public void HandleTokenValidationNotification(
            TokenValidationNotification _)
        {
            _isGitLabAccessTokenValid = false;
        }

        /// <summary>
        /// Call before setting multiple settings. This
        /// prevents the SettingsChangedEvent from being
        /// triggered on each individual settings change.
        /// </summary>
        public void StartBatchSettingsUpdate()
        {
            _batchSettingsUpdate = true;
        }

        /// <summary>
        /// Called to stop batch mode and send a 
        /// SettingsChangedEvent.
        /// </summary>
        public void StopBatchSettingsUpdate()
        {
            _batchSettingsUpdate = false;
            Storage.Save(this);
            OnSettingsChanged(ApplicationName);
        }

        /// <summary>
        /// GitLab Access Token
        /// </summary>
        public string GitLabAccessToken
        {
            get { return GitlabAccessTokenValue; }
            set
            {
                GitlabAccessTokenValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabAccessTokenKey);
                }
            }
        }

        /// <summary>
        /// Is code suggestions enabled
        /// </summary>
        public bool IsCodeSuggestionsEnabled
        {
            get { return IsCodeSuggestionsEnabledValue; }
            set
            {
                IsCodeSuggestionsEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsCodeSuggestionsEnabledKey);
                }
            }
        }

        public bool IsTelemetryEnabled
        {
            get { return IsTelemetryEnabledValue; }
            set
            {
                IsTelemetryEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsTelemetryEnabledKey);
                }
            }
        }

        /// <summary>
        /// GitLab URL
        /// </summary>
        public string GitLabUrl
        {
            get { return GitLabUrlValue; }
            set
            {
                GitLabUrlValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabUrlKey);
                }
            }
        }

        /// <summary>
        /// Logging level. Defualts to Warning
        /// </summary>
        public LogEventLevel LogLevel
        {
            get { return LogLevelValue; }
            set
            {
                LogLevelValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(LogLevelKey);
                }
            }
        }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        public bool Configured
        {
            get
            {
                return (!string.IsNullOrEmpty(GitLabUrl)) && 
                    (!string.IsNullOrEmpty(GitLabAccessToken)) &&
                    _isGitLabAccessTokenValid;
            }
        }

        /// <summary>
        /// Raise the SettingsChangedEvent
        /// </summary>
        private void OnSettingsChanged(string changedSettingKey)
        {
            // Don't trigger a SettingsChangedEvnet
            // if we are batch updating settings.
            if (_batchSettingsUpdate)
                return;

            // assume access token is valid until we hear something from the language server indicating otherwise
            _isGitLabAccessTokenValid = true;

            var settingsChangedEvent = SettingsChangedEvent;
            if (settingsChangedEvent == null)
                return;

            settingsChangedEvent(this, new SettingsEventArgs(changedSettingKey));
        }

        public void Dispose()
        {
            _tokenValidationNotificationsSubscription.Dispose();
        }
    }
}
