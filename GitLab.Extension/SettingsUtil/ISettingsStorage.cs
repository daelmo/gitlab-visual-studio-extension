﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.SettingsUtil
{
    public interface ISettingsStorage
    {
        void Load(Settings settings);
        void Save(Settings settings);
    }
}
