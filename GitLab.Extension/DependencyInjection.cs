﻿using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status;
using AutofacSerilogIntegration;
using System.Reactive.Subjects;
using System;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.InfoBar;
using GitLab.Extension.VS;
using GitLab.Extension.Command;
using GitLab.Extension.Workspace;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Language.Suggestions;

namespace GitLab.Extension
{
    public class DependencyInjection
    {
        private static DependencyInjection _instance = null;
        private static ISettings _settings = null;

        public static DependencyInjection Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DependencyInjection();

                return _instance;
            }
        }

        public ILifetimeScope Scope { get; }

        private DependencyInjection()
        {
            var container = RegisterComponents();
            Scope = container.BeginLifetimeScope();

            _settings = Scope.Resolve<ISettings>();
        }

        private IContainer RegisterComponents()
        {
            var builder = new ContainerBuilder();

            // Commands
            builder.RegisterCommands();

            // CodeSuggestions

            builder.RegisterType<GitlabProposalSource>();
            builder.RegisterType<GitlabProposalManager>();
            builder.RegisterType<LanguageManager>()
                .As<ILanguageManager>()
                .SingleInstance();

            // CodeSuggestion - Telemetry

            builder.RegisterType<SuggestionServiceTelemetryAdaptor>()
                .SingleInstance()
                .AutoActivate();

            builder.RegisterType<GitLabCodeSuggestionTelemetryController>()
                .As<IGitLabCodeSuggestionTelemetryController>();

            builder.Register(c =>
            {
                var components = GlobalServiceProvider.GetService(typeof(SComponentModel)) as IComponentModel;
                return components?.GetService<SuggestionServiceBase>();
            });

            // Workspace

            builder.RegisterType<LsClientProvider>()
                .As<IWorkspaceLsClientProvider>();

            // LanguageServer

            builder.RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            builder.RegisterType<LsClientTs>()
                .As<ILsClient>();
            builder.RegisterType<LsProcessManagerTs>()
                .As<ILsProcessManager>()
                .SingleInstance();
            builder.RegisterType<LsClientRpc>();

            // LanguageServerGlobalEvents

            builder.Register(c => new Subject<TokenValidationNotification>())
                .As<IObservable<TokenValidationNotification>>()
                .As<IObserver<TokenValidationNotification>>()
                .SingleInstance();

            // Settings

            builder.RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            builder.RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();
            builder.RegisterType<ProtectImpl>()
                .As<ISettingsProtect>()
                .SingleInstance();

            // Status

            builder.RegisterType<Status.StatusBar>()
                .SingleInstance();
            builder.RegisterType<Status.ExtensionStatusControl>()
                .As<IStatusControl>()
                .SingleInstance();

            // Logging

            builder.RegisterLogger();

            // InfoBar
            builder.RegisterType<InfoBarFactory>()
                .As<IInfoBarFactory>()
                .SingleInstance();

            // Notification Service
            builder.RegisterType<InvalidTokenNotificationService>()
                .AutoActivate();

            return builder.Build();
        }
    }
}
