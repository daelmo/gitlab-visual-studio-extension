﻿using System;

namespace GitLab.Extension.InfoBar
{

    public interface IInfoBar
    {
        void Close();
        event EventHandler Closed;
    }
}
