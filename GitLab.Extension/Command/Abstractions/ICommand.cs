﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Command
{
    public interface ICommand
    {
        Task InitializeAsync(
            IServiceProvider serviceProvider);
    }
}
