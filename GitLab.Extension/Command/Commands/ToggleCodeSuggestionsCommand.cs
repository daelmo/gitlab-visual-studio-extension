﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.VS;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace GitLab.Extension.Command
{
    public class ToggleCodeSuggestionsCommand : ICommand
    {
        private static readonly CommandID ToggleCodeSuggestionsCommandId = 
            new CommandID(
                PackageIds.GitLabPackageCmdSet, 
                PackageIds.GL_CMD_TOGGLE_CODE_SUGGESTIONS);
        
        private readonly ISettings _settings;

        public ToggleCodeSuggestionsCommand(
            ISettings settings)
        {
            _settings = settings;
        }

        public async Task InitializeAsync(
            IServiceProvider serviceProvider)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            var oleMenuItem = 
                new OleMenuCommand(
                    ToggleCodeSuggestions, 
                    ToggleCodeSuggestionsCommandId);

            var commandService = 
                serviceProvider.GetService(typeof(IMenuCommandService)) as IMenuCommandService;
            
            commandService?.AddCommand(oleMenuItem);
        }

        private void ToggleCodeSuggestions(
            object obj,
            EventArgs args)
        {
            _settings.IsCodeSuggestionsEnabled = !_settings.IsCodeSuggestionsEnabled;
        }
    }
}
