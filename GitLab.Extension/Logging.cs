﻿using Serilog;
using Serilog.Enrichers.Sensitive;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows;
using GitLab.Extension.Serilog.Sink.VsOutput;
using GitLab.Extension.SettingsUtil;
using Serilog.Events;
using Serilog.Core;
using Autofac;

namespace GitLab.Extension
{
    public static class Logging
    {
        private static ISettings _settings;
        private static readonly LoggingLevelSwitch _loggingLevelSwitch;

        private static object _loggingLock = new object();
        private static UIElement _uiElement = null;
        private static LogEventLevel _minimumLevel = LogEventLevel.Warning;
        private static ILogEventSink _extraSink = null;
        private static bool _configured = false;

        public static string TracePrefix = "GlVsExt";


        static Logging()
        {
            _loggingLevelSwitch = new LoggingLevelSwitch();
            _loggingLevelSwitch.MinimumLevel = _minimumLevel;
        }

        public static LogEventLevel MinimumLevel
        {
            get
            {
                return _minimumLevel;
            }
            set
            {
                _minimumLevel = value;
                _loggingLevelSwitch.MinimumLevel = _minimumLevel;
            }
        }
        private static void SettingsChangedEvent(object sender, EventArgs e)
        {
            MinimumLevel = _settings.LogLevel;
        }

        public static void TestingOnlyConfigureLogging(ILogEventSink extraSink = null)
        {
            _configured = false;
            ConfigureLogging(extraSink);
        }

        public static void ConfigureLogging(ILogEventSink extraSink = null)
        {
            lock (_loggingLock)
            {
                try
                {
                    if (_configured)
                        return;

                    _configured = true;
                    _uiElement = Application.Current?.MainWindow;

                    if (extraSink != null)
                        _extraSink = extraSink;

                    var logger = new LoggerConfiguration()
                        .Enrich.WithSensitiveDataMasking(options =>
                        {
                            options.MaskingOperators = new List<IMaskingOperator>()
                            {
                                new GitLabPersonalAccessTokenMaskingOperator()
                            };
                        })
                        .Enrich.FromLogContext()
                        .MinimumLevel.ControlledBy(_loggingLevelSwitch);
#if DEBUG
                    logger.WriteTo.Trace(outputTemplate: $"{TracePrefix} [{{Level}}] {{Message}} {{Exception}}");
#endif

                    if(_extraSink != null)
                        logger.WriteTo.Sink(_extraSink);

                    if (_uiElement != null)
                        logger.WriteTo.VsOutput(_uiElement);

                    Log.Logger = logger.CreateLogger();

                    _settings = DependencyInjection.Instance.Scope.Resolve<ISettings>();
                    _settings.SettingsChangedEvent += SettingsChangedEvent;
                    MinimumLevel = _settings.LogLevel;

                    // Use error so message is always displayed
                    Log.Error($"Current logging level set to '{MinimumLevel}'. Logging level can be changed in the 'GitLab' options.");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"{TracePrefix} Error in Logging.ConfigureLogging: {ex}");
                }
            }
        }
    }

    // <summary>
    // Mask PAT in log output
    // </summary>
    public class GitLabPersonalAccessTokenMaskingOperator : RegexMaskingOperator
    {
        private const string PatPattern = "glpat-.{20}";

        public GitLabPersonalAccessTokenMaskingOperator() :
            base(PatPattern, RegexOptions.Compiled)
        {

        }

        protected override string PreprocessMask(string mask, Match match)
        {
            return $"glpat-{mask}";
        }
    }
}
