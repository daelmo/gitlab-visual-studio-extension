﻿using GitLab.Extension.InfoBar;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension
{
    public class InvalidTokenNotificationService : IDisposable
    {
        public const string INVALID_ACCESS_TOKEN_MESSAGE = "Invalid access token. To continue using code suggestions from GitLab, update your token.";
        public const string OPEN_SETTINGS_OPTION_LABEL = "Open Settings";

        private readonly ISettings _settings;
        private readonly IInfoBarFactory _infoBarFactory;
        private readonly IObservable<TokenValidationNotification> _tokenValidationNotifications;
        private readonly IDisposable _tokenValidationNotificationSubscription;

        private IInfoBar _infoBar = null;

        public InvalidTokenNotificationService(
            ISettings settings,
            IInfoBarFactory infoBarFactory,
            IObservable<TokenValidationNotification> tokenValidationNotifications)
        {
            _settings = settings;
            _infoBarFactory = infoBarFactory;
            _tokenValidationNotifications = tokenValidationNotifications;
            _tokenValidationNotificationSubscription = _tokenValidationNotifications.Subscribe(HandleTokenValidationNotification);

            _settings.SettingsChangedEvent += HandleSettingsUpdated;
        }

        private void HandleSettingsUpdated(
            object sender, 
            EventArgs e)
        {
            _infoBar?.Close();
        }

        private void HandleTokenValidationNotification(
            TokenValidationNotification tokenValidation)
        {
            const string message = INVALID_ACCESS_TOKEN_MESSAGE;

            var l = new Func<Task>(async () =>
            {
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

                if (_infoBar != null)
                {
                    // we're already notifying the user.
                    return;
                }

                _infoBar = _infoBarFactory.AttachInfoBarToMainWindow(
                    message,
                    new List<InfoBarAction>
                    {
                        new InfoBarAction(OPEN_SETTINGS_OPTION_LABEL, () =>
                        {
                            GitLabOptionsPackageActions.ShowOptionsPage();
                            _infoBar?.Close();
                        })
                    });

                _infoBar.Closed += (i, args) => 
                {
                    ThreadHelper.JoinableTaskFactory.Run(async delegate
                    {
                        await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                        _infoBar = null;
                    });
                };
            });

            _ = l.Invoke();
        }

        public void Dispose()
        {
            _settings.SettingsChangedEvent -= HandleSettingsUpdated;
            _tokenValidationNotificationSubscription.Dispose();
        }
    }
}
